from flair.data import Corpus
from flair.models.text_classification_model import TARSClassifier
from flair.trainers import ModelTrainer
from flair.datasets import CSVClassificationCorpus

column_name_map = {0: "label", 1: "text"}

tasks_list = ('topic_huffpost_oneword',
              'topic_huffpost_interpretation', 'topic_huffpost_wordnet')
task = iter(tasks_list)

#########################
corpus: Corpus = CSVClassificationCorpus(
    column_name_map=column_name_map,
    data_folder='topic_huffpost/oneword',
    test_file=None,
    skip_header=True,
    delimiter=','
)

tars = TARSClassifier(task_name=next(
    task), label_dictionary=corpus.make_label_dictionary())

trainer = ModelTrainer(tars, corpus)

trainer.train(base_path='topic_huffpost/model',
              learning_rate=0.02,
              mini_batch_size=16,
              mini_batch_chunk_size=4,
              max_epochs=10,
              train_with_test=True)
#########################

# continue train on alternative label names 
data_folders = ['topic_huffpost/interpretation', 'topic_huffpost/wordnet']

for data_folder in data_folders:
    new_corpus: Corpus = CSVClassificationCorpus(
        column_name_map=column_name_map,
        data_folder=data_folder,
        test_file=None,
        skip_header=True,
        delimiter=','
    )

    tars.add_and_switch_to_new_task(
        next(task), label_dictionary=new_corpus.make_label_dictionary())

    trainer = ModelTrainer(tars, new_corpus)

    trainer.train(base_path='topic_huffpost',  # path to store the model artifacts
                  learning_rate=0.02,  # use very small learning rate
                  mini_batch_size=16, # optionally set this if transformer is too much for your machine
                  mini_batch_chunk_size=4,
                  max_epochs=10,
                  train_with_test=True  # terminate after 10 epochs
                  )