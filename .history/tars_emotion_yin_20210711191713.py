from flair.data import Corpus
from flair.models.text_classification_model import TARSClassifier
from flair.trainers import ModelTrainer
from flair.datasets import CSVClassificationCorpus

column_name_map = {0: "label", 1: "text"}

tasks_list = ('emotion_yin_oneword',
              'emotion_yin_interpretation', 'emotion_yin_wordnet')
task = iter(tasks_list)
model_path = 'emotion_yin'

#########################
corpus: Corpus = CSVClassificationCorpus(
    column_name_map=column_name_map,
    data_folder='emotion_yin/oneword',
    test_file=None,
    skip_header=True,
    delimiter=','
)

tars = TARSClassifier(task_name=next(
    task), label_dictionary=corpus.make_label_dictionary())

trainer = ModelTrainer(tars, corpus)

trainer.train(base_path='emotion_yin',
              learning_rate=0.02,
              mini_batch_size=16,
              mini_batch_chunk_size=4,
              max_epochs=10,
              train_with_test=True)
#########################

# continue train on alternative label names 
data_folders = ['emotion_yin/interpretation', 'emotion_yin/wordnet']

for data_folder in data_folders:
    new_corpus: Corpus = CSVClassificationCorpus(
        column_name_map=column_name_map,
        data_folder=data_folder,
        test_file=None,
        skip_header=True,
        delimiter=','
    )

    tars.add_and_switch_to_new_task(
        next(task), label_dictionary=new_corpus.make_label_dictionary())

    trainer = ModelTrainer(tars, new_corpus)

    trainer.train(base_path='emotion_yin/model',  # path to store the model artifacts
                  learning_rate=0.02,  # use very small learning rate
                  mini_batch_size=16, # optionally set this if transformer is too much for your machine
                  mini_batch_chunk_size=4,
                  max_epochs=10,
                  train_with_test=True  # terminate after 10 epochs
                  )